// All the relevant web pages.
var pages = ["index", "families/targaryen", "families/stark", "continents/westeros", "continents/essos", "extra/plot"];

// When the document is ready:
$(document).ready(function () {
    // Load Pages
    var div = document.createElement("div");
    div.style.display = "none";
    document.getElementsByTagName("main")[0].appendChild(div);
    for (var i = 0; i < pages.length; i++) {
        load_page(div, pages[i], "../");
    }
    window.setTimeout(makeHTML, 1000);
});

function makeHTML() {
    for (var i = 0; i < pages.length; i++) {
        var path = pages[i].split("/");
        var html = $("#" + path.last())[0].contentDocument;
        var page = new WebPage(html);
        switch (path[0]) {
            case "families":
                page = new Family(html);
                break;
            case "continents":
                page = new Continent(html);
                break;
            case "extra":
                page = new Extra(html);
                break;
            default:
                page = new WebPage(html);
        }
        $("main")[0].appendChild(page.toHTML());
    }
}

class WebPage {
    constructor(html) {
        this.title = html.getElementsByTagName("h1")[0].innerHTML;
        this.keywords = html.getElementsByTagName("meta").keywords.content;
        this.description = html.getElementsByTagName("meta").description.content;
        this.path = html.URL;
    }

    toHTML() {
        var section = document.createElement("section");
        section.setAttribute("class", "summary");
        var header = document.createElement("h2");
        var link = document.createElement("a");
        link.setAttribute("href", path);
        var text = document.createTextNode(this.title);
        link.appendChild(text);
        header.appendChild(link);
        section.appendChild(header);
        var keys = document.createElement("p");
        var key = document.createElement("b");
        key.appendChild(document.createTextNode("Keywords: "));
        keys.appendChild(key);
        keys.appendChild(document.createTextNode(this.keywords));
        section.appendChild(keys);
        var desc = document.createElement("p");
        key = document.createElement("b");
        key.appendChild(document.createTextNode("Description: "));
        desc.appendChild(key);
        desc.appendChild(document.createTextNode(this.description));
        section.appendChild(desc);
        return section;
    }
}

class Family extends WebPage {
    constructor(html) {
        super(html);
        this.introduction = html.getElementById("introduction");
        this.introduction.removeChild(this.introduction.firstElementChild);
        this.houseBanner = html.getElementById("houseBanner");
    }

    toHTML() {
        var section = super.toHTML();
        this.houseBanner.style = "margin-right: 3%";
        section.innerHTML = this.houseBanner.outerHTML + section.innerHTML;
        section.appendChild(this.introduction);
        return section;
    }
}

class Continent extends WebPage {
    constructor(html) {
        super(html);
        this.introduction = html.getElementById("introduction");
        this.introduction.removeChild(this.introduction.firstElementChild);
        this.map = html.getElementById("locMap");
    }

    toHTML() {
        var section = super.toHTML();
        this.map.style = "margin-right: 3%";
        section.innerHTML = this.map.outerHTML + section.innerHTML;
        section.appendChild(this.introduction);
        return section;
    }
}

class Extra extends WebPage {
    constructor(html) {
        super(html);
        this.introduction = document.createElement("section");
        var p = document.createElement("p");
        p.appendChild(document.createTextNode("This page shows a graph of all the named people that have died in the A Song of Ice and Fire novels."));
        this.introduction.appendChild(p);
    }

    toHTML() {
        var section = super.toHTML();
        section.appendChild(this.introduction);
        return section;
    }
}