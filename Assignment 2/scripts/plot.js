//The data that is shown in the graph
var deathlist = [["Ser Waymar Royce", "Will", "Gared", "Lord Jon Arryn", "Mycah", "Lady", "Ser Hugh Of The Vale", "Jyk", "Kurleket", "Mohor", "Morrec", "Chiggen", "Lharys", "Heward", "Wyl", "Jory Cassel", "Hali", "Wallen", "Stiv", "Tregar", "Ser Vardis Egen", "Viserys Targaryen", "King Robert Baratheon", "Fat Tom", "Varly", "Cayn", "Desmond", "Hullen", "Porther", "Othor", "Jafer Flowers", "Lord Vance", "Ser Raymun Darry", "Masha Heddle", "Ser Jaremy Rykker", "Khal Ogo", "Khal Fogo", "Ulf Son Of Ulmar", "Conn Son Of Coratt", "Lord Halys Hornwood", "Eddard Karstark", "Torrhen Karstark", "Daryn Hornwood", "Quaro", "Qotho", "Haggo", "Cohollo", "Lord Eddard Stark", "Septa Mordane", "Vayon Poole", "Rhaego", "Eroeh", "Khal Drogo", "Lord Andros Brax", "Mirri Maz Duur"],
["Maester Cressen", " Praed", " Ser Burton Crakehall", " Lord Lyman Darry", " Barra", " Allar Deem", " Doreah", " Woth", " Dobber", " Qyle", " Murch", " Koss", " Gerren", " Kurz", " Reysen", " Urreg", " Yoren", " Lommy Greenhands", " King Renly Baratheon", " Ser Robar Royce", " Ser Emmon Cuy", " Ser Stevron Frey", " Ser Stafford Lannister", " Chiswyck", " Benfred Tallhart", " Weese", " High Septon (Fat One)", " Ser Preston Greenfield", " Ser Aron Santagar", " Alfyn Crowkiller", " Ser Cortnay Penrose", " Lord Leo Lefford", " Lady Donella Hornwood", " Alebelly", " Mikken", " Ser Amory Lorch", " Septon Chayle", " Drennan", " Squint", " Gelmarr The Grim", " Aggar", " Gynir Rednose", " Farlen", " Ser Imry Florent", " Ser Mandon Moore", " Lord Bryce Caron", " Ser Guyard Morrigen", " Maester Tothmure", " Ser Rodrik", " Cley Cerwyn", " Leobald Tallhart", " Red Rolfe", " Kenned", " Ulf", " Black Lorren", " Squire Dalbridge", " Ebben", " Qhorin Halfhand", " Poxy Tym", " Maester Luwin"],
["Iggo", " Ser Jacelyn Bywater", " Allard Seaworth", " Dale Seaworth", " Matthos Seaworth", " Maric Seaworth", " Jate Blackberry", " Lord Guncer Sunglass", " Lord Chyttering", " Hookface Will", " Hal The Hog", " Maslyn", " Thoren Smallwood", " Ser Ottyn Wythers", " Small Paul", " Ser Helman Tallhart", " Tion Frey", " Willem Lannister", " Delp", " Elwood", " Lord Rickard Karstark", " Lord Monford Velaryon", " Kraznys mo Nakloz", " Grazdan mo Ullhor", " Ser Cleos Frey", " Jarl", " Symon Silvertongue", " Brown Bernarr", " Bannen", " Craster", " Garth Of Oldtown", " Rolley Of Sisterton", " Ser Byam Flint", " Lord Commander Jeor Mormont", " Alyn", " Lord Hoster Tully", " King Balon Greyjoy", " Lord Sawane Botley", " Kyle", " Septon Utt", " Bodger", " Lark The Sisterman", " Ryles", " Chett", " Softfoot", " Prendahl na Ghezn", " Sallor The Bald", " Robin Flint", " Ser Wendel Manderly", " Lucas Blackwood", " Donnel Locke", " Owen Norrey", " Ser Garse Goodbrook", " Lady Dacey Mormont", " Smalljon Umber", " Aegon 'Jinglebell' Frey", " King Robb Stark", " Ser Tytos Frey", " Ser Raynald Westerling", " Grey Wind", " Deaf Dick Follard", " Rast", " Young Henly", " Old Henly", " Dornish Dilly", " Magnar Styr", " Quort", " Stone Thumbs", " Ygritte", " Oznak zo Pahl", " Mero", " King Joffrey Lannister", " Ser Dontos Hollard", " Vargo Hoat", " Donal Noye", " Mag The Mighty", " Ser Endrew Tarth", " Ser Aladale Wynch", " Red Alyn Of The Rosewood", " Prince Oberyn Nymeros Martell", " Orell", " Harma The Dogshead", " Polliver", " The Tickler", " Watt Of Longlake", " Dalla", " Shae", " Lord Tywin Lannister", " Lord Eon Hunter", " Lysa Arryn", " Petry Frey", " Merrett Frey"],
["Pate", " Nimble Dick Crabb", " Pyg", " Timeon", " Shagwell The Fool", " Senelle", " Ser Arys Oakheart", " Marillion", " Lord Baelor Blacktyde", " Ser Talbert Serry", " Ser Balman Byrch", " Falyse Stokeworth", " Dareon", " Maester Aemon", " Lady Tanda Stokeworth", " Rorge", " Biter", " Lord Gyles Rosby", " Lord Beric Dondarrion", " Ser Ryman Frey"],
["Thistle", " Varamyr Sixskins", " Stalwart Shield", " Hazzea", " Janos Slynt", " Dirk", " Ollo Lophand", " Clubfoot Karl", " Lord Alester Florent", " Kyra", " Rattleshirt", " Mossador", " Ralf Kenning", " Dagon Codd", " Adrack Humble", " Hagen The Horn", " Rolfe The Dwarf", " Quenton Greyjoy", " Oppo", " Black Jack Bulwer", " Hairy Hal", " Garth Greyfeather", " Ser Jared Frey", " Rhaegar Frey", " Symond Frey", " Lord Harwood Fell", " Yellow Dick", " Little Walder Frey", " Luton", " Holly", " Barsena Blackhair", " Yurkhaz zo Yunzak", " Dormund", " Torwynd The Tame", " Hamish The Harper", " Maester Kerwin", " Nurse", " Admiral Groleo", " Yezzan zo Qaggaz", " Cromm", " Crunch", " Khrazz", " Rowan", " Squirrel", " Willow Witch-Eye", " Frenya", " Myrtle", " Ser Patrek Of King's Mountain", " Prince Quentyn Martell", " Grand Maester Pycelle", " Ser Kevan Lannister"]];

var startData = [
    { label: "Deaths per book", data: [["A Game of Thrones", deathlist[0].length], ["A Clash of Kings", deathlist[1].length], ["A Storm of Swords", deathlist[2].length], ["A Feast for Crows", deathlist[3].length], ["A Dance with Dragons", deathlist[4].length]] }
];

//Sets the options for both the main graph and the overview graph
var options = {
    series: {
        bars: {
            barWidth: 0.8,
            align: "center",
            horizontal: false,
            show: true
        },
        lines: {
            show: false
        }
    },
    xaxis: {
        mode: "categories",
        tickLength: 0
    },
    selection: {
        mode: "xy"
    },
    grid: {
        hoverable: true,
        clickable: true
    }
};

var optionsOver = {
    legend: {
        show: false
    },
    series: {
        bars: {
            barWidth: 0.6,
            align: "center",
            horizontal: false,
            show: true
        },
        lines: {
            show: false
        }
    },
    xaxis: {
        mode: "categories",
        tickLength: 0
    },
    selection: {
        mode: "xy"
    }
};

window.onload = function () {
    var placeholder = $("#placeholder");
    var plot = $.plot(placeholder, startData, options);

    var overview = $.plot("#overview", startData, optionsOver);

    //Defines the style of and adds the tooltip that shows up when the user hovers over a bar
    $("<div id='tooltip'></div>").css({
        position: "absolute",
        display: "none",
        border: "1px solid #fdd",
        padding: "2px",
        "background-color": "#fee",
        opacity: 0.80
    }).appendTo("body");

    //If the user hovers over a bar in the graph, the number of deaths is shown
    $("#placeholder").bind("plothover", function (event, pos, item) {
        if ($("#enablePosition:checked").length > 0) {
            var str = "(" + pos.x.toFixed(2) + ", " + pos.y.toFixed(2) + ")";
            $("#hoverdata").text(str);
        }
        if ($("#enableTooltip:checked").length > 0) {
            if (item) {
                var x = item.dataIndex,
                    y = item.datapoint[1];
                $("#tooltip").html("Number of deaths in " + item.series.data[x][0] + " = " + y)
                    .css({top: item.pageY+5, left: item.pageX+5})
                    .fadeIn(200);
            } else {
                $("#tooltip").hide();
            }
        }
    });

    //If the user clicks on a bar in the graph, the deaths in that book are shown
    $("#placeholder").bind("plotclick", function (event, pos, item) {
        if (item) {
            makeList(deathlist[item.dataIndex]);
        }
    });


    //Shows the dimensions of the graph when resized
    placeholder.resize(function () {
        $(".message").text("The graph is now " + $(this).width() + "x" + $(this).height() + " pixels");
    });

    //Makes it possible that the graphs are resizable
    $(".graph").resizable({
        maxWidth: 1000,
        maxHeight: 500,
        minWidth: 450,
        minHeight: 250
    });

    //Connects the overview and main graph
    placeholder.bind("plotselected", function (event, ranges) {
        //Zooms the main graph
        if (ranges.xaxis.to - ranges.xaxis.from < 0.00001) {
            ranges.xaxis.to = ranges.xaxis.from + 0.0001;
        }

        if (ranges.yaxis.to - ranges.yaxis.from < 0.00001) {
            ranges.yaxis.to = ranges.yaxis.from + 0.00001;
        }

        plot = $.plot(placeholder, startData, $.extend(true, {}, options, {
            xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
            yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
        }));

        overview.setSelection(ranges, true);
    });

    $("#overview").bind("plotselected", function (event, ranges) {
        plot.setSelection(ranges);
    });

    $("footer").append("Flot " + $.plot.version);
};

//Turns an array into an unordered list
function makeList(list) {
    var div = document.getElementById("clickdata");
    var ul = div.children[0];
    ul.innerHTML = "";

    for (var i = 0; i < list.length; i++) {
        var li = document.createElement('li');
        li.appendChild(document.createTextNode(list[i]));
        ul.appendChild(li);
    }
};