// Sets the text color of the entire body
function setTextColor(color) {
    document.getElementsByTagName("body")[0].style.color = color;
}

// Gets the html currently selected and keeps layout.
function getHTMLSelection() {
    // Get the selected elements
    var selection = window.getSelection();
    if (selection.rangeCount > 0) {
        // Get the content of selection
        var clone = selection.getRangeAt(0).cloneContents();
        // If the selected text was plain text
        if (clone.childElementCount == 0) {
            // Get the tag it was part of and use that for formatting
            var outer = document.createElement(selection.anchorNode.parentNode.tagName);
            outer.appendChild(clone);
            return outer.outerHTML;
        }
        // Convert content to html
        var div = document.createElement("div");
        div.appendChild(clone);
        return div.innerHTML;
    }
    return "";
}

function load_page(target, file, root) {
    var object = document.createElement("object");
    var path = file.split("/");
    object.setAttribute("id", path.last());
    object.setAttribute("type", "text/html");
    object.setAttribute("data", root + file + ".html");
    target.appendChild(object);
}

// Puts html after the end of the target(or if whitspace is selected between the closest objects).
function putHTML(target, html) {
    if (target.tagName == "MAIN" || target.tagName == "SECTION" || target.tagName == "BODY") {
        for (var i = 0; i < target.children.length; i++)
            if (target.children[i].getBoundingClientRect().y > window.pasteY) {
                target.children[i].insertAdjacentHTML("beforebegin", html);
                break;
            }
    }
    else
        target.insertAdjacentHTML("afterend", html);
}

if (!Array.prototype.last) {
    Array.prototype.last = function () {
        return this[this.length - 1];
    };
};