// When the document is ready:
$(document).ready(function () {
    // Create Context Menu
    var menu = document.createElement("ul");
    menu.setAttribute("class", "custom-menu");

    // Create bold/normal option
    var option1 = document.createElement("li");
    option1.appendChild(document.createTextNode("Bold"));
    option1.setAttribute("data-action", "bold");

    // Create copy option
    var option2 = document.createElement("li");
    option2.appendChild(document.createTextNode("Copy"));
    option2.setAttribute("data-action", "copy");

    // Create paste option
    var option3 = document.createElement("li");
    option3.appendChild(document.createTextNode("Paste"));
    option3.setAttribute("data-action", "paste");

    // Create text color picker
    var option4 = document.createElement("li");
    option4.setAttribute("data-action", "col");
    option4.appendChild(document.createTextNode("Text Color: "));
    var colorpick = document.createElement("input");
    colorpick.setAttribute("id", "colorPicker");
    colorpick.setAttribute("type", "Color");
    colorpick.setAttribute("onchange", "setTextColor(document.getElementById('colorPicker').value); $('.custom-menu').hide(100);");
    option4.appendChild(colorpick);

    // Add options to menu
    menu.appendChild(option1);
    menu.appendChild(option2);
    menu.appendChild(option3);
    menu.appendChild(option4);

    // Add menu to document
    document.body.appendChild(menu);
});

// Trigger action when the contexmenu is about to be shown
$(document).bind("contextmenu", function (event) {
    // Avoid the real one
    event.preventDefault();

    //Save position for pasting
    window.pasteX = event.clientX;
    window.pasteY = event.clientY;

    // Show contextmenu at correct position
    $(".custom-menu").finish().toggle(100).css({
        top: event.pageY + "px",
        left: event.pageX + "px"
    });
});


// If the document is clicked somewhere
$(document).bind("mousedown", function (e) {

    // If the clicked element is not the menu
    if ($(e.target).parents(".custom-menu").length > 0) {
        // Switch on the targets data action
        switch ($(e.target).attr("data-action")) {
            // A case for each action.
            case "bold":
                //  Make text normal
                if (window.bold) {
                    document.body.style.fontWeight = "initial";
                    window.bold = false;
                    e.target.innerHTML = "Bold";
                }
                // Make the text bold
                else {
                    document.body.style.fontWeight = "bold";
                    window.bold = true;
                    e.target.innerHTML = "Normal";
                }
                $(".custom-menu").hide(100);
                return;
            case "copy":
                window.copy = getHTMLSelection();
                $(".custom-menu").hide(100);
                return;
            case "paste":
                putHTML(document.elementsFromPoint(window.pasteX, window.pasteY)[0], window.copy);
                $(".custom-menu").hide(100);
                return;
            case "col":
                document.getElementById('colorPicker').value = "#000000";
                setTextColor("#000000");
                return;
            default:
                return;
        }
    }
    // Hide it
    $(".custom-menu").hide(100);
});