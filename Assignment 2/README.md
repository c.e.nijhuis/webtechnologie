# Readme

## Changes we have made since Assignment 1

* We have changed the video at the home page (index.html) to use YouTube.
* Formatted the HTML with a formatter
* Added Script tags for javascript files.
* Implemented the requirements for Assignment 2