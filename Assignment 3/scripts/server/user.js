var sqlite3 = require("sqlite3").verbose();
var crypto = require("crypto");
exports.insertQuery = function(db, user) {
	var stmt = db.prepare("INSERT INTO Userdata VALUES (?,?,?,?,?)");
	var values = [user.password, user.firstName, user.lastName, user.email];
	stmt.run(values);
	stmt.finalize();
}
exports.loginQuery = function(db, email, funct) {
	var sql = "SELECT password FROM Userdata WHERE email=?";
	db.get(sql, [email], funct);
}
