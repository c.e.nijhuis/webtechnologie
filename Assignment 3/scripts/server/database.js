var fs = require("fs");
var file = __dirname + "/../../database/" + "database.db";
var exists = fs.existsSync(file);
if (!exists) {
    fs.openSync(file, "w");
}
var sqlite3 = require("sqlite3").verbose();
var users = require("./user.js");
var products = require("./products.js");
var crypto = require("crypto");

function open() {
  return new sqlite3.Database(file, (err) => {
    if (err) {
      return console.error(err.message);
    }
    console.log('Connected to the database');
  });
}

function close(db) {
  db.close((err) => {
	if (err) {
	  return console.error(err.message);
	}
	console.log('Closed the database connection');
  });	
}

if(!exists)
{
	var db = open();

	db.serialize(function() {
		createdatabase();
	});

	close(db);
	exists = true;
}

//Creates and fills the initial database if it does not exist 
function createdatabase() {
    db.run("CREATE TABLE Productdata (title TEXT, description TEXT, category TEXT, manufacturer TEXT, price INTEGER, image TEXT, productid INTEGER PRIMARY KEY)");
    db.run("CREATE TABLE Userdata (password TEXT, firstname TEXT, lastname TEXT, email TEXT, userid INTEGER PRIMARY KEY)");
    db.run("CREATE TABLE History (date TEXT, userid INTEGER, productid INTEGER, PRIMARY KEY (userid, productid))");
    
    var stmt = db.prepare("INSERT INTO Productdata VALUES (?,?,?,?,?,?,?)");
    
    for (var i = 0; i < products.products.length; i++){
        stmt.run(products.products[i]);
    }
    stmt.finalize();
}

exports.getProduct = function(query){
	var db = open();
	var sql = "SELECT title FROM Productdata WHERE category=?";
	db.each(sql, [query], (err, row) => {
		if (err) {
			throw err;
		}
		console.log(`${row.title}`);
	});
	close(db);
}

exports.getData = function(sql) {
    var db = open();
    var rows = [];
    db.each(sql, function(err, row) => {
        if (err)
            throw err;
        rows.push(row);
    }
    close(db);
    return rows;
}

exports.addUser = function(userData) {
	var db = open();
	users.insertQuery(db, userData);
	close(db);
}

exports.login = function(email, funct) {
	var db = open();
	users.loginQuery(db, email, funct);
	close(db);
}