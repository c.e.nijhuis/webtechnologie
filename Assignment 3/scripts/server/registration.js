var database = require("./database.js");
var crypto = require("crypto");
var session = require('express-session');
var cookieParser = require('cookie-parser');

exports.register_post = function(req, res, next) {
	var user = new User(req.body.user.first, req.body.user.last, req.body.user.password, req.body.user.email);
	database.addUser(user);
	res.redirect("/group14/index.html");
    createSession(req, res);
};

exports.login_post = function(req, res, next) {	
	database.login(req.body.email, (err, row) => {
		if(err)
		{
			console.error(err.message);
			res.redirect("/group14/pages/login.html");
			return;
		}
		if(row)
		{
			var hash = crypto.createHash('sha256').update(req.body.password).digest('hex');
			if(row.password == hash)
			{
				res.redirect("/group14/index.html");
                next();
				return;
			}
		}
		res.redirect("/group14/pages/login.html");
	});
}

class User {
	constructor(first, last, pass, email) {
		this.firstName = first;
		this.lastName = last;
		this.password = crypto.createHash('sha256').update(pass).digest('hex');
		this.email = email;
	}
}
