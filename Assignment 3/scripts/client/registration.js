$(document).ready(function(){
	$("#registration")[0].addEventListener("submit", passCheck);
});

function passCheck(event) {
  var pass1 = $("#password")[0].value;
  var pass2 = $("#passwordconfirm")[0].value;
  if(pass1 != pass2) {
    $("#password")[0].style.borderColor = "red";
    $("#passwordconfirm")[0].style.borderColor = "red";
    var error = document.createElement("p");
    error.setAttribute("class", "error");
    error.appendChild(document.createTextNode("Passwords don't match"));
    $("#registration")[0].appendChild(error);
    event.preventDefault();
  }
}
