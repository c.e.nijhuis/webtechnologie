window.addEventListener("load", loadProducts, false);
window.addEventListener("load", (function(){document.getElementById("more").addEventListener("click", loadProducts, false);}), false);

function loadProducts(){
    var url = "scripts/server/getproducts.js";
    console.log("hello");
    
    get(url, function(req){
        console.log(req.responseText);
        //document.getElementById("productcontainer").innerHTML = req.responseText;
    });
}

function get(url, funct){
    var req;
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTPP");
    }
    else {
        alert("your browser doe not support XMLHTTP!");
    }
    
    req.open("GET",url, true);
    req.onreadystatechange = function () {
        if (req.readyState === 4 && req.status === 200) {
            funct(req);
        }
    }
    req.send();
}