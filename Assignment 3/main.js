#!/usr/bin/env nodejs

var express = require('express');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var parse = require('url').parse;
var join = require('path').join;
var fs = require('fs');
var logger = require('logger-request');
var app = express();
const bodyParser = require("body-parser");
var register = require('./scripts/server/registration.js');
var database = require('./scripts/server/database.js');

app.use(cookieParser());
app.set('trust proxy', 1) // trust first proxy
app.use(session({
  secret: "Hotred is the best!",
  resave: false,
  saveUninitialized: true,
  cookie: {secure: true, maxAge: 600000, path: '/'}
}));

app.get('getproducts.js', function (req, res) {
    if (req.session.i)
        req.session.i++;
    else
        req.session.i = 1;
    var sql = "SELECT * FROM Productdata ORDER BY title ASC LIMIT " + req.session.i;
    res.send(database.getData(sql));
});
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(logger({filename:'request.log'}));
app.get("/protected_pages/*", checkSignIn, function(req, res){
        res.render(req.url, {id: req.session.user.id})
    });
app.use("/", express.static(__dirname));
app.post("/pages/registration.html", register.register_post, createSession);
app.post("/pages/login.html", register.login_post, createSession);
app.use(function(req, res) {
  res.status(404).send("Page not found!");
});
app.listen(8005); //NOTE: Uncomment when using pm2
module.exports=app;

function createSession(req, res)
{
    var newUser = {email: req.body.email, password: req.body.password};
    req.session.user = newUser;
    console.log(req.session.user);
}

function checkSignIn(req, res, next){
   console.log(req.session.user);
   if(req.session.user){
      next();
   } else {
      console.log("Not logged in");
      res.redirect("/group14/pages/login.html");
   }
}